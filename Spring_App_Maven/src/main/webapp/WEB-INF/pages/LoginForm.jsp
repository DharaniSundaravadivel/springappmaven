<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login/Register</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
    <div class="container" align="center">
    <div class="jumbotron jumbotron-info" >
        <h1>Login/Register</h1>
        <form:form action="userLogin" method="post" modelAttribute="user">
        <table>
            <form:hidden path="id"/>
            <tr>
                <td>User Name</td>
                <td><form:input path="username" /><form:errors path="username"></form:errors></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><form:input path="password" /><form:errors path="password"></form:errors></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="submit" value="Submit"></td>
            </tr>
        </table>
        </form:form>
    </div>
    </div>
</body>
</html>
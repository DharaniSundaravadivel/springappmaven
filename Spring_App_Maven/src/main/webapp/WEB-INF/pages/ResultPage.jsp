<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quiz Result</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<h1 align="center">Quiz Result</h1>
<div class="container">
<table class="table table-bordered">
<tr>
<td>Total Number Of Questions</td>
<td>10</td>
</tr>
<tr class="success">
<td>Number Of Questions - Correct</td>
<td>6</td>
</tr>
<tr class="danger">
<td>Number Of Questions - Wrong</td>
<td>4</td>
</tr>
</table>
</div>
</body>
</html>
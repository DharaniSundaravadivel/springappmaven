package com.quiz.Service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import com.quiz.Model.Questions;
import com.quiz.Model.User;


public interface UserService {
    
	public List<Questions> getQuestions();
}

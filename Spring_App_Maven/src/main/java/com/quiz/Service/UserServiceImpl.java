package com.quiz.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quiz.Dao.UserDao;
import com.quiz.Model.Questions;
import com.quiz.Model.User;
@Service
@Transactional
public class UserServiceImpl implements UserService{
	@Autowired
	UserDao dao;
	@Transactional
	public List<Questions> getQuestions() {
		return dao.getQuestions();
	}
}

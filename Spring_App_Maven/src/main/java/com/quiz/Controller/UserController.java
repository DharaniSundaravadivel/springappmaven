package com.quiz.Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.quiz.Model.Questions;
import com.quiz.Model.User;
import com.quiz.Service.UserService;

@Controller
public class UserController {
	@Autowired
	UserService service;
	@RequestMapping(value = "Login")
	public ModelAndView LoginUser(@ModelAttribute("user") User user) throws IOException {
		return new ModelAndView("LoginForm");
	}
	@RequestMapping(value = "/userLogin" , method=RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute("user") @Valid User user,BindingResult result) throws IOException {
			if(result.hasErrors()){
				return new ModelAndView("LoginForm");
			}
			else{
			List<Questions> listQuestions = service.getQuestions();
			return new ModelAndView("AttemptQuiz","listQuestions", listQuestions);
			}
	}
	@RequestMapping(value = "/userResult")
	public ModelAndView ResultUser(@ModelAttribute("user") User user) throws IOException {
		return new ModelAndView("ResultPage");
	}
}
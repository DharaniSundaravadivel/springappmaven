package com.spg.Dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.spg.Model.Employee;


public interface EmployeeDao {

	public void addEmployee(Employee employee);
	public List<Employee> getAllEmployees();
	public Employee getEmployee(int employeeId);
	public void updateEmployee(Employee employee);
	public void deleteEmployee(int employeeId);
}

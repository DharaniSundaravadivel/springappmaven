package com.spg.Dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spg.Model.Employee;
@Repository
public class EmployeeDaoImpl implements EmployeeDao {
	@Autowired
	private SessionFactory sessionFactory;
	public void addEmployee(Employee employee) {
		 sessionFactory.getCurrentSession().save(employee);
	}
	@SuppressWarnings("unchecked")
	public List<Employee> getAllEmployees() {

		return sessionFactory.getCurrentSession().createQuery("from Employee")
				.list();
	}
	public Employee getEmployee(int employeeId){
		return(Employee) sessionFactory.getCurrentSession().get(Employee.class, employeeId);
		
	}
	public void updateEmployee(Employee employee) {
		sessionFactory.getCurrentSession().update(employee);
	}
	@Override
	public void deleteEmployee(int employeeId) {
		Employee employee = (Employee) sessionFactory.getCurrentSession().load(Employee.class, employeeId);
		if (null != employee) {sessionFactory.getCurrentSession().delete(employee);}
	}
}

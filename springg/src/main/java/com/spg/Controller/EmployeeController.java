package com.spg.Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.spg.Model.Employee;
import com.spg.Service.EmployeeService;

@Controller
public class EmployeeController {
	@Autowired
	EmployeeService service;
	@RequestMapping(value = "Login")
	public ModelAndView LoginEmployee(@ModelAttribute("employee") Employee employee) throws IOException {
		return new ModelAndView("EmployeeForm");
	}
	@RequestMapping(value = "/saveEmployee" , method=RequestMethod.POST)
	public ModelAndView saveEmployee(@ModelAttribute("employee") @Valid Employee employee,BindingResult result) throws IOException {
			if(result.hasErrors()){
				return new ModelAndView("EmployeeForm");
			}
			if(employee.getId()==0){
				service.addEmployee(employee);
				List<Employee> listEmployee = service.getAllEmployees();
				return new ModelAndView("Success","listEmployee", listEmployee);	
			}
			service.updateEmployee(employee);
			List<Employee> listEmployee = service.getAllEmployees();
			return new ModelAndView("Success","listEmployee", listEmployee);
	}
	@RequestMapping(value = "/editEmployee", method=RequestMethod.GET)
	public ModelAndView editEmployee(HttpServletRequest request){
		int employeeId= Integer.parseInt(request.getParameter("id"));
		Employee employee=service.getEmployee(employeeId);
		return new ModelAndView("EmployeeForm","employee",employee);
	}
	@RequestMapping(value = "/deleteEmployee", method = RequestMethod.GET)
	public ModelAndView deleteEmployee(HttpServletRequest request) {
		int employeeId = Integer.parseInt(request.getParameter("id"));
		service.deleteEmployee(employeeId);
		List<Employee> listEmployee = service.getAllEmployees();
		return new ModelAndView("Success","listEmployee", listEmployee);
	}

}
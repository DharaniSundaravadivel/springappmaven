package com.spg.Service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import com.spg.Model.Employee;


public interface EmployeeService {
    
	public void addEmployee(Employee employee);
	public List<Employee> getAllEmployees();
	public Employee getEmployee(int employeeId);
	public void updateEmployee(Employee employee);
	public void deleteEmployee(int employeeId);

}

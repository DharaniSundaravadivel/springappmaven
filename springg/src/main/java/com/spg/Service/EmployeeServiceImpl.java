package com.spg.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spg.Dao.EmployeeDao;
import com.spg.Model.Employee;
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{
	@Autowired
	EmployeeDao dao;
	public void addEmployee(Employee employee){
		dao.addEmployee(employee);
	}
	@Transactional
	public List<Employee> getAllEmployees() {
		return dao.getAllEmployees();
	}
	public Employee getEmployee(int employeeId){
		return dao.getEmployee(employeeId);
	}
	public void updateEmployee(Employee employee) {
		dao.updateEmployee(employee);
	}
	@Override
	public void deleteEmployee(int employeeId) {
		dao.deleteEmployee(employeeId);	
	}
}

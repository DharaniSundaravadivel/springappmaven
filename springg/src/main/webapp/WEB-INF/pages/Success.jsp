<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Success Page</title>
<style>
#table2{
border-collapse:collapse;
}
th{
background-color:cyan;
}
</style>
</head>
<body>
	<div align="center">
		<h1>Employee List</h1>
		
<table id="table2" border="1">
<tr>
	<th>ID</th>
	<th>Name</th>
	<th>Email</th>
	<th>Address</th>
	<th>Telephone</th>
	<th>Action</th>
</tr>
			<c:forEach var="employee" items="${listEmployee}">

				<tr>
					<td>${employee.id}</td>
					<td>${employee.name}</td>
					<td>${employee.email}</td>
					<td>${employee.address}</td>
					<td>${employee.telephone}</td>
					
					<td><a href="editEmployee?id=${employee.id}">Edit</a>
						&nbsp;&nbsp;
						<a href="deleteEmployee?id=${employee.id}">Delete</a></td>
				</tr>
			</c:forEach>
</table>
		<h4>
			<a href="Login">Add New Employee</a>
		</h4>
	</div>
</body>
</html>